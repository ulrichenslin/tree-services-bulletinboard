package com.korwe.tree.bulletinBoard.service;

import com.korwe.thecore.annotation.ParamNames;
import com.korwe.thecore.service.PingService;
import java.util.List;
import com.korwe.tree.bulletinBoard.dto.NoticeBoard;
import com.korwe.tree.bulletinBoard.dto.Notice;
import com.korwe.tree.bulletinBoard.dto.Comment;


/**
 * This file was generated using Kordapt
 */

public interface BulletinService extends PingService{

    @ParamNames("communityId")
    public List<NoticeBoard> listNoticeBoards(String communityId);

    @ParamNames("noticeBoardId")
    public List<Notice> listNotices(String noticeBoardId);

    @ParamNames("noticeId")
    public List<Comment> listComments(String noticeId);

    @ParamNames("id")
    public NoticeBoard deleteNoticeBoard(String id);

    @ParamNames("id")
    public Notice deleteNotice(String id);

    @ParamNames("id")
    public Comment deleteComment(String id);

    @ParamNames({"id", "name", "communityId"})
    public NoticeBoard createUpdateNoticeBoard(String id, String name, String communityId);

    @ParamNames({"id", "noticeBoardId", "title", "description"})
    public Notice createUpdateNotice(String id, String noticeBoardId, String title, String description);

    @ParamNames({"id", "noticeId", "comment"})
    public Comment createUpdateComment(String id, String noticeId, String comment);
}