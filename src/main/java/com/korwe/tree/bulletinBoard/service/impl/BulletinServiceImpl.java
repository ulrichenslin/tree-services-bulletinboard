package com.korwe.tree.bulletinBoard.service.impl;

import com.korwe.thecore.service.ping.PingServiceImpl;
import java.util.List;
import com.korwe.tree.bulletinBoard.dto.NoticeBoard;
import com.korwe.tree.bulletinBoard.dto.Notice;
import com.korwe.tree.bulletinBoard.dto.Comment;
import com.korwe.tree.bulletinBoard.service.BulletinService;



/**
 * This file was generated using Kordapt
 */

public class BulletinServiceImpl extends PingServiceImpl implements BulletinService{

        @Override
        public List<NoticeBoard> listNoticeBoards(String communityId){
            //TODO: Implement function listNoticeBoards
            return null;
        }

        @Override
        public List<Notice> listNotices(String noticeBoardId){
            //TODO: Implement function listNotices
            return null;
        }

        @Override
        public List<Comment> listComments(String noticeId){
            //TODO: Implement function listComments
            return null;
        }

        @Override
        public NoticeBoard deleteNoticeBoard(String id){
            //TODO: Implement function deleteNoticeBoard
            return null;
        }

        @Override
        public Notice deleteNotice(String id){
            //TODO: Implement function deleteNotice
            return null;
        }

        @Override
        public Comment deleteComment(String id){
            //TODO: Implement function deleteComment
            return null;
        }

        @Override
        public NoticeBoard createUpdateNoticeBoard(String id, String name, String communityId){
            //TODO: Implement function createUpdateNoticeBoard
            return null;
        }

        @Override
        public Notice createUpdateNotice(String id, String noticeBoardId, String title, String description){
            //TODO: Implement function createUpdateNotice
            return null;
        }

        @Override
        public Comment createUpdateComment(String id, String noticeId, String comment){
            //TODO: Implement function createUpdateComment
            return null;
        }
}