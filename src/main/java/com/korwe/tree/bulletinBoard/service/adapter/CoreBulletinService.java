package com.korwe.tree.bulletinBoard.service.adapter;

import com.korwe.kordapt.KordaptCoreService;
import com.thoughtworks.xstream.XStream;
import com.korwe.tree.bulletinBoard.service.BulletinService;


/**
 * This file was generated using Kordapt
 */
public class CoreBulletinService extends KordaptCoreService<BulletinService>{
    public CoreBulletinService(BulletinService delegate, String serviceName, int maxThreads) {
        super(delegate, serviceName, maxThreads, false);
    }

    public CoreBulletinService(BulletinService delegate, String serviceName, int maxThreads, XStream xStream) {
        super(delegate, serviceName, maxThreads, xStream, false);
    }
}
