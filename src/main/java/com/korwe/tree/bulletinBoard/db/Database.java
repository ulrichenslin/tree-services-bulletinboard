package com.korwe.tree.bulletinBoard.db;

import org.mapdb.*;

import java.io.File;
import java.util.concurrent.ConcurrentNavigableMap;

/**
 * Created by ulrich1 on 15/08/07.
 */
public class Database {

    static DB db;
    ConcurrentNavigableMap<String, String> map;

    Database(String collection) {
        map = getDb().treeMap(collection);
    }

    static DB getDb() {
        if (db == null) {
            db = DBMaker.fileDB(new File("testdb"))
                    .closeOnJvmShutdown()
                    .encryptionEnable("password")
                    .make();
        }
        return db;
    }

    void put(String key, String value) {
        map.put(key, value);
    }

    static void commit() {
        getDb().commit();
    }


}
