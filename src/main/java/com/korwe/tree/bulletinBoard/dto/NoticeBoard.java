package com.korwe.tree.bulletinBoard.dto;


/**
 * This file was generated using Kordapt
 */

public class NoticeBoard {
    private String id;
    private String name;
    private String communityId;

    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getCommunityId(){
        return communityId;
    }

    public void setCommunityId(String communityId){
        this.communityId = communityId;
    }


}