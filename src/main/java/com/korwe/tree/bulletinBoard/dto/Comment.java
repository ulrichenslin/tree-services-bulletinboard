package com.korwe.tree.bulletinBoard.dto;


/**
 * This file was generated using Kordapt
 */

public class Comment {
    private String id;
    private String noticeId;
    private String comment;

    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getNoticeId(){
        return noticeId;
    }

    public void setNoticeId(String noticeId){
        this.noticeId = noticeId;
    }

    public String getComment(){
        return comment;
    }

    public void setComment(String comment){
        this.comment = comment;
    }


}