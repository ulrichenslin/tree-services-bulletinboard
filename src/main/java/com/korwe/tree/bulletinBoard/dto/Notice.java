package com.korwe.tree.bulletinBoard.dto;


/**
 * This file was generated using Kordapt
 */

public class Notice {
    private String id;
    private String noticeBoardId;
    private String title;
    private String description;

    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getNoticeBoardId(){
        return noticeBoardId;
    }

    public void setNoticeBoardId(String noticeBoardId){
        this.noticeBoardId = noticeBoardId;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getDescription(){
        return description;
    }

    public void setDescription(String description){
        this.description = description;
    }


}